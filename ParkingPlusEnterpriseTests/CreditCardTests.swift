//
//  CreditCardTests.swift
//  ParkingPlusEnterpriseTests
//
//  Created by Duarte, Anderson on 06/10/2018.
//  Copyright © 2018 Duarte, Anderson. All rights reserved.
//

import XCTest
@testable import ParkingPlusEnterprise

class CreditCardTests: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }

    func testCreate() {
        let creditCard = CreditCard(issuer: .VISA, number: "1234123412341234", validity: "122020", verificationValue: "8280", holderDocument: "12345678901", holderName: "Alan Vondore", encrypt: true)
        XCTAssertEqual(creditCard.issuer, .VISA)
        XCTAssertEqual(creditCard.number, "1234123412341234")
        XCTAssertEqual(creditCard.validity, "122020")
        XCTAssertEqual(creditCard.verificationValue, "8280")
        XCTAssertEqual(creditCard.holderDocument, "12345678901")
        XCTAssertEqual(creditCard.holderName, "Alan Vondore")
        XCTAssertEqual(creditCard.encrypt, true)
        XCTAssertNil(creditCard.encryptedValue)
    }
    
    func testCreateEncrypted() {
        let creditCard = CreditCard(issuer: CreditCard.Issuer.VISA, encryptedValue: "123abc", holderDocument: "12345678901", holderName: "Alan Vondore")
        XCTAssertEqual(creditCard.issuer, .VISA)
        XCTAssertEqual(creditCard.encryptedValue, "123abc")
        XCTAssertEqual(creditCard.holderDocument, "12345678901")
        XCTAssertEqual(creditCard.holderName, "Alan Vondore")

    }
    
    func testIssuerDetection() {
        var issuer = CreditCard.Issuer.detect(cardNumber: "5078601870000127985")
        XCTAssertEqual(issuer, CreditCard.Issuer.AURA)
        issuer = CreditCard.Issuer.detect(cardNumber: "376347810515321")
        XCTAssertEqual(issuer, CreditCard.Issuer.AMERICAN_EXPRESS)
        issuer = CreditCard.Issuer.detect(cardNumber: "6011187834067372")
        XCTAssertEqual(issuer, CreditCard.Issuer.DISCOVER)
        issuer = CreditCard.Issuer.detect(cardNumber: "36006666333344")
        XCTAssertEqual(issuer, CreditCard.Issuer.DINERS_CLUB)
        issuer = CreditCard.Issuer.detect(cardNumber: "5066991111111118")
        XCTAssertEqual(issuer, CreditCard.Issuer.ELO)
        issuer = CreditCard.Issuer.detect(cardNumber: "3553204901892375")
        XCTAssertEqual(issuer, CreditCard.Issuer.JCB)
        issuer = CreditCard.Issuer.detect(cardNumber: "6062828888666688")
        XCTAssertEqual(issuer, CreditCard.Issuer.HIPERCARD)
        issuer = CreditCard.Issuer.detect(cardNumber: "5377083771653821")
        XCTAssertEqual(issuer, CreditCard.Issuer.MASTERCARD)
        issuer = CreditCard.Issuer.detect(cardNumber: "4518780868012968")
        XCTAssertEqual(issuer, CreditCard.Issuer.VISA)
    }

}
