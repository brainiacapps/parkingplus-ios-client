//
//  ParkingPlusEnterpriseTests.swift
//  ParkingPlusEnterpriseTests
//
//  Created by Duarte, Anderson on 04/09/2018.
//  Copyright © 2018 Duarte, Anderson. All rights reserved.
//

import XCTest
@testable import ParkingPlusEnterprise

class ParkingPlusEnterpriseTests: XCTestCase {
    
    var api : ParkingPlus!

    override func setUp() {
        super.setUp()
//        let configuration = URLSessionConfiguration.ephemeral
//        configuration.protocolClasses = [MockURLProtocol.self]
//        let urlSession = URLSession(configuration: configuration)
        api = ParkingPlus(config: (UDID: "123", API_KEY: "wps2@18pofe12g5412",
                                   API_HOST: "https://homologacao.parkingplus.com.br/servicos/2"))
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testPayment() throws {
        let expectation = self.expectation(description: "Payment")
        let creditCard = CreditCard(issuer: .VISA, number: "4916308674816113", validity: "122022",
                                    verificationValue: "884", holderDocument: "82762715881", holderName: "Alan Vodore")
        
        api.pay(ticket: "038983339900", with: creditCard, amount: 1) { result in
            switch result {
            case .success(let result):
                XCTAssertNotNil(result)
                XCTAssertNotNil(result.checkOutDate)
                XCTAssertNotNil(result.paymentDate)
                XCTAssertNotNil(result.receipt)
                XCTAssertTrue(result.success)
                expectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testListPayments() throws {
        let expectation = self.expectation(description: "ListPayments")
        api.listPayments() { result in
            switch result {
            case .success(let payments):
                XCTAssertNotNil(payments)
                XCTAssertNotNil(payments.count)
                XCTAssertGreaterThan(payments.count, 0)
                let payment = payments[0]
                XCTAssertNotNil(payment.ticketNumber)
                XCTAssertNotNil(payment.date)
                XCTAssertNotNil(payment.amountPaid)
                print("Payment Date: \(payment.date)")
                expectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testListPromotions() throws {
        let expectation = self.expectation(description: "ListPayments")
        api.listPromotions(ticketNumber: "038983339900", parkingLotCode: 1) { result in
            switch result {
            case .success(let promotions):
                XCTAssertNotNil(promotions)
                XCTAssertGreaterThan(promotions.count, 0)
                let promotion = promotions[0]
                XCTAssertNotNil(promotion.title)
                XCTAssertNotNil(promotion.timeBegin)
                XCTAssertNotNil(promotion.timeEnd)
                expectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testGetTicketInfoExistent() throws {
//        let bundle = Bundle(for: type(of: self))
//        let path = bundle.path(forResource: "ticket-valid", ofType: "json")!
//        let mockData = NSData(contentsOfFile: path)! as Data
//        MockURLProtocol.requestHandler = { request in
//            let url = request.url!
//            let condition = url.absoluteString.contains("038983339900")
//            XCTAssertTrue(condition)
//            return (HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!, mockData)
//        }
        let expectation = self.expectation(description: "getTicketInfoExistent")
        api.getTicketInfo(number: "038983339900") { result in
            switch result {
            case .success(let ticket):
                XCTAssertNotNil(ticket.number)
                XCTAssertNotNil(ticket.fare)
                XCTAssertTrue(ticket.isValid)
                expectation.fulfill()
            case .failure(let error):
                XCTFail(error.message!)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testGetTicketInfoNonExistent() {
        let expectation = self.expectation(description: "getTicketInfoNonExistent")
//        MockURLProtocol.requestHandler = { request in
//            let condition = request.url!.absoluteString.contains("38983339900")
//            XCTAssertTrue(condition)
//            return (HTTPURLResponse(), "".data(using: .utf8)!)
//        }
        api.getTicketInfo(number: "38983339900") { result in
            switch result {
            case .success:
                XCTFail("Ticket should not exist.")
                expectation.fulfill()
            case .failure(let error):
                XCTAssertNotNil(error)
                XCTAssertNotNil(error.localizedDescription)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5)
    }

    func testPerformanceExample() {
        self.measure {
        }
    }
    
}
