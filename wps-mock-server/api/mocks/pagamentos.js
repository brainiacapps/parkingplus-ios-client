'use strict';
var chance = require('chance').Chance();

module.exports = {
    pagamentosEfetuadosUsingGET: pagamentosEfetuadosUsingGET
}

function pagamentosEfetuadosUsingGET(req, res) {
    console.log(req.swagger.path.get.responses['200'].schema);
    var pagamentos = [];
    for(var i = 0; i < 10; i++) {
        pagamentos[i] = {
            'cnpjGaragem': chance.string({ length: 16, pool: '0123456789' }), 
            'data': new Date().getMilliseconds(), 
            'estaticonamento': chance.name(), 
            'valorPago': chance.integer({ min: 100, max: 1000 })
        } 
    }
    res.json(pagamentos);
}