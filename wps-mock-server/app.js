'use strict';
process.env.DEBUG = 'swagger:*';
var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);
  
  console.log('Running on http://127.0.0.1:' + port);
  // console.log(swaggerExpress.runner.swagger.paths);

});
