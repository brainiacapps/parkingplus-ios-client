//
//  ParkingLog.swift
//  ParkingPlusEnterprise
//
//  Created by Duarte, Anderson on 19/09/2018.
//  Copyright © 2018 Duarte, Anderson. All rights reserved.
//

import Foundation

public class ParkingLot : Decodable {
    
    /** ID da Garagem: idGaragem */
    public var code: Int64?
    /** Nome da Garagem associada ao ticket: garagem */
    public var name: String?
    /** CNPJ da Garagem: cnpjGaragem */
    public var document: String?
    /** Link de uma imagem para o estacionamento */
    public var logoLink: String?
    
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)
        self.code = try container.decodeIfPresent(Int64.self, forKey: "idGaragem")
        self.name = try container.decodeIfPresent(String.self, forKey: "garagem")
        self.document = try container.decodeIfPresent(String.self, forKey: "cnpjGaragem")
        self.logoLink = try container.decodeIfPresent(String.self, forKey: "linkLogoGaragem")
    }

}
