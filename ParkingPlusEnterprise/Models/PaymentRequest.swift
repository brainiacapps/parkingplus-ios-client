import Foundation



class PaymentRequest: Encodable {

    public let creditCard : CreditCard
    /** Número do código de barras do ticket do estacionamento */
    public let ticketNumber: String
    /** Identificador único para o dispositivo */
    public let udid: String
    /** Valor do pagamento em centavos. Exemplo: para um pagamento de R$1,50 enviar 150 */
    public let amount: Int
    /** Endereco IP do dispositivo */
    public let ipAddress: String
    /** Identificação única para a transação */
    public let transactionId: String
    
    /** ID da Garagem referente ao ticket do estacionamento.
     Caso não utilizado o sistema tentará buscar o ticket em todas as garagens.
     Retorna erro se mais de um ticket for encontrado */
    public var parkingLotCode: Int64?
    /** ID da promoção que será utilizada */
    public var promotionCode: Int64?
    /** Notas para serem utilizadas em uma promoção */
    public var invoices: [String]?
    
    public init(creditCard: CreditCard, ticketNumber: String, amount: Int, ipAddress: String, udid: String) {
        self.creditCard = creditCard
        self.transactionId = UUID().uuidString
        self.ipAddress = ipAddress
        self.ticketNumber = ticketNumber
        self.udid = udid
        self.amount = amount
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: String.self)
        try container.encode(self.creditCard.issuer.rawValue, forKey: "bandeira")
        if let creditCardEncrypted = self.creditCard.encryptedValue {
            try container.encode(creditCardEncrypted, forKey: "cartaoCriptografado")
        }
        else if let number = creditCard.number, let verificationValue = creditCard.verificationValue,
            let validity = creditCard.validity, let encrypt = creditCard.encrypt {
            try container.encode(number, forKey: "cartaoDeCredito")
            try container.encode(verificationValue, forKey: "codigoDeSeguranca")
            try container.encode(encrypt, forKey: "criptografarCartao")
            try container.encode(validity, forKey: "validade")
        }
        try container.encode(creditCard.holderDocument, forKey: "cpfCnpj")
        try container.encode(creditCard.holderName, forKey: "portador")
        try container.encode(transactionId, forKey: "idTransacao")
        try container.encode(ticketNumber, forKey: "numeroTicket")
        try container.encode(udid, forKey: "udid")
        try container.encode(amount, forKey: "valor")
        try container.encode(ipAddress, forKey: "enderecoIp")

        try container.encodeIfPresent(parkingLotCode, forKey: "idGaragem")
        try container.encodeIfPresent(promotionCode, forKey: "idPromocao")
        try container.encodeIfPresent(invoices, forKey: "notas")
    }
    
    public func getKey(_ apiKey: String) -> String {
        return ticketNumber + udid + ipAddress + creditCard.issuer.rawValue + creditCard.holderName + transactionId + apiKey
    }
}
