public class PaymentResult: Decodable {

    /** O número do ticket */
    public let ticketNumber: String
    /** Se o ticket foi pago */
    public let success: Bool
    /** Texto com o comprovante de pagamento do ticket */
    public let receipt: String
    /** Data e hora permitida para a saída do estacionamento(Unix Epoch em milissegundos) */
    public let checkOutDate: Date
    /** Data do servidor na hora do pagamento(Unix Epoch em milissegundos) */
    public let paymentDate: Date
    /** O número do RPS gerado */
    public let rps: String?
    /** A série do RPS */
    public let serieRps: String?
    /** O cartão criptografado se solicitado */
    private (set) public var creditCardEncrypted: String?
    private (set) public var error : ParkingPlusError

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)
        ticketNumber = try container.decode(String.self, forKey: "numeroTicket")
        success = try container.decode(Bool.self, forKey: "ticketPago")
        receipt = try container.decode(String.self, forKey: "comprovante")
        checkOutDate = try container.decode(Date.self, forKey: "dataHoraSaida")
        paymentDate = try container.decode(Date.self, forKey: "dataPagamento")
        
        creditCardEncrypted = try container.decodeIfPresent(String.self, forKey: "cartaoCriptografado")
        rps = try container.decodeIfPresent(String.self, forKey: "rps")
        serieRps = try container.decodeIfPresent(String.self, forKey: "serieRps")
        error = try ParkingPlusError(from: decoder)
    }
}

