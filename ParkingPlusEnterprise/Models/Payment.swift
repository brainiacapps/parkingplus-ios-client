import Foundation

public class Payment: Decodable {

    public enum Kind: String, Decodable {
        case cargaCredito = "CARGA_CREDITO"
        case cartaoDebito = "CARTAO_DEBITO"
        case ticket = "TICKET"
    }

    public let ticketNumber : String
    /** Data do servidor na hora do pagamento(Unix Epoch em milissegundos) */
    public let date: Date
    public var paidAt : String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter.string(from: self.date)
    }
    /** Valor do pagamento */
    public let amountPaidRaw: Int64
    public var amountPaid : String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter.string(from: NSNumber(value: Double(amountPaidRaw) / 100.0))!
    }
    public let checkInDate : Date
    public let checkOutDate : Date
    public var stayTime : String {
        let timeinterval = self.date.timeIntervalSince(self.checkInDate)
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.allowedUnits = [.minute]
        return formatter.string(from: timeinterval)!
    }
    public let amountDiscount : Int64
    public let isPrePaid : Bool

    /** CPF enviado para geração de RPS no pagamento */
    public let payerDocument: String?
    private (set) public var parkingLot : ParkingLot?
    
    public let authorizationCode : String?
    public let nfseVerificationCode : String?
    public let nfseNumber : String?
    public let nfseQrCode : String?
    public let invoiceNSU : String?
    /** O número do RPS gerado */
    public let rps: String?
    /** A série do RPS */
    public let serieRps: String?
    /** O tipo do serviço pago. */
    public let kind: Kind?

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)
        
        ticketNumber =  try container.decode(String.self, forKey: "ticket")
        amountPaidRaw = try container.decode(Int64.self, forKey: "valorPago")
        date = try container.decode(Date.self, forKey: "data")
        isPrePaid = try container.decode(Bool.self, forKey: "prepago")
        checkInDate = try container.decode(Date.self, forKey: "permanencia")
        checkOutDate = try container.decode(Date.self, forKey: "permanenciaFim")
        amountDiscount = try container.decode(Int64.self, forKey: "valorDesconto")

        payerDocument = try container.decodeIfPresent(String.self, forKey: "cpfCnpj")
        parkingLot = try ParkingLot(from: decoder)
        authorizationCode = try container.decodeIfPresent(String.self, forKey: "codigoAutorizacao")
        nfseVerificationCode = try container.decodeIfPresent(String.self, forKey: "nfseCodigoVerificacao")
        nfseNumber = try container.decodeIfPresent(String.self, forKey: "nfseNumero")
        nfseQrCode = try container.decodeIfPresent(String.self, forKey: "nfseQrCode")
        invoiceNSU = try container.decodeIfPresent(String.self, forKey: "nsu")
        rps = try container.decodeIfPresent(String.self, forKey: "rps")
        serieRps = try container.decodeIfPresent(String.self, forKey: "serieRps")
        kind = try container.decodeIfPresent(Kind.self, forKey: "tipo")
    }
}
