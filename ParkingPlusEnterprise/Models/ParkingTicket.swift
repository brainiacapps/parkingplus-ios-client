import Foundation

public class ParkingTicket: Decodable {

    /** O número do ticket */
    public var number: String
    /** Valor TOTAL da tarifa do ticket em centavos(R$1,50 &#x3D; 150). Este valor ignora o valor já pago, mas não ignora descontos */
    public var fare: Int
    /** Valor da tarifa já paga em centavos(R$1,50 &#x3D; 150). Para calcular o valor a pagar utilize tarifa - tarifaPaga */
    public var farePaid: Int
    /** Valor TOTAL e sem desconto da tarifa do ticket em centavos(R$1,50 &#x3D; 150). */
    public var fareWithoutDiscount: Int
    /** Se o ticket consultado é válido */
    public var isValid: Bool
    /** Valor do desconto aplicado na tarifa em centavos(R$1,50 = 150) */
    public var discount: Int
    /** Data do servidor no momento da consulta(Unix Epoch em milissegundos) */
    public var dateValidation: Date
    /** Data de entrada(Unix Epoch em milissegundos) */
    public var dateCheckIn: Date
    /** Uma previsão da data permitida para saída se o ticket for pago com o valor atual (Unix Epoch em milissegundos) */
    public var dateCheckOutMax: Date?

    public var fareDueFormatted : String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        let value = NSNumber(value: Double(self.fare / 100) - Double(self.farePaid / 100))
        return formatter.string(from: value)!
    }
    
    public var fareDue : Int {
        return self.fare - self.farePaid
    }
    
    public var hasDueValue : Bool {
        return fareDue > 0
    }
    
    public var stayTime : String {
        let timeinterval = dateValidation.timeIntervalSince(self.dateCheckIn)
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.allowedUnits = [.minute]
        return formatter.string(from: timeinterval)!
    }
    
    public var allowsRecurrence: Bool?
    /** Se a promoção existe e foi atingida */
    public var isPromotionAchieved: Bool?
    /** Se existem promoções cadastradas para a garagem do ticket */
    public var hasPromotionsAvailable: Bool?
    public var recorrente: Bool?
    /** Setor do ticket */
    public var sector: String?
    /** O id da promoção(O mesmo do request) */
    public var promotionId: Int64?
    /** Link de uma imagem para o estacionamento */
    public var imageLink: String?
    /** Lista com as informações sobre as notas enviadas */
    public var invoices: [Invoice]?
    public var creditCardMasked: String?
    public var creditCardRequestCVV: Bool?
    public var creditCardValidationMessage: String?
    public var parkingLot : ParkingLot?
    public var error : ParkingPlusError?
    

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)

        number = try container.decode(String.self, forKey: "numeroTicket")
        fare = try container.decode(Int.self, forKey: "tarifa")
        farePaid = try container.decode(Int.self, forKey: "tarifaPaga")
        fareWithoutDiscount = try container.decode(Int.self, forKey: "tarifaSemDesconto")
        isValid = try container.decode(Bool.self, forKey: "ticketValido")
        discount = try container.decode(Int.self, forKey: "valorDesconto")
        dateValidation = try container.decode(Date.self, forKey: "dataConsulta")
        dateCheckIn = try container.decode(Date.self, forKey: "dataDeEntrada")
        dateCheckOutMax = try container.decodeIfPresent(Date.self, forKey: "dataPermitidaSaida")
        
        promotionId = try container.decodeIfPresent(Int64.self, forKey: "idPromocao")
        imageLink = try container.decodeIfPresent(String.self, forKey: "imagemLink")
        invoices = try container.decodeIfPresent([Invoice].self, forKey: "notas")
        allowsRecurrence = try container.decodeIfPresent(Bool.self, forKey: "permiteRecorrencia")
        isPromotionAchieved = try container.decodeIfPresent(Bool.self, forKey: "promocaoAtingida")
        hasPromotionsAvailable = try container.decodeIfPresent(Bool.self, forKey: "promocoesDisponiveis")
        recorrente = try container.decodeIfPresent(Bool.self, forKey: "recorrente")
        sector = try container.decodeIfPresent(String.self, forKey: "setor")
        error = try ParkingPlusError(from: decoder)
        parkingLot = try ParkingLot(from: decoder)
        creditCardMasked = try container.decodeIfPresent(String.self, forKey: "cartaoMascarado")
        creditCardValidationMessage = try container.decodeIfPresent(String.self, forKey: "mensagemValidacao")
        creditCardRequestCVV = try container.decodeIfPresent(Bool.self, forKey: "solicitarCodigoSeguranca")
    }
}

