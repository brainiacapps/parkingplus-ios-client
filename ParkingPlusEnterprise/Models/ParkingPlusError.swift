
public class ParkingPlusError: Decodable, Error {

    public let code: Int?
    public let message: String?
    
    public init(code: Int, message: String) {
        self.code = code
        self.message = message
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)
        code = try container.decodeIfPresent(Int.self, forKey: "errorCode")
        message = try container.decodeIfPresent(String.self, forKey: "mensagem")
    }
}

