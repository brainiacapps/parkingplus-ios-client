import Foundation

open class TicketRefreshRequest: Encodable {

    /** Número do código de barras do ticket do estacionamento */
    public var ticketNumber: String
    /** Identificador único para o usuário */
    public var udid: String
    /** ID da promoção que será utilizada */
    public var promotionId: Int64?
    /** ID da Garagem referente ao ticket do estacionamento. Caso não utilizado o sistema tentará buscar o ticket em todas as garagens. Retorna erro se mais de um ticket for encontrado */
    public var parkingLotCode: Int64?
    /** Notas para serem utilizadas em uma promoção */
    public var invoices: [String]?
    /** Lista de tipos de promoção aceitos (SIMPLE &#x3D; Qualquer promoção que precise somente do ID para ser utilizada) */
    public var promotionsKind: [String]?
    
    public init(ticketNumber: String, udid: String, parkingLotCode: Int64? = nil, promotionId: Int64? = nil, invoices: [String]? = nil, promotionsKind: [String]? = nil) {
        self.parkingLotCode = parkingLotCode
        self.promotionId = promotionId
        self.invoices = invoices
        self.ticketNumber = ticketNumber
        self.promotionsKind = promotionsKind
        self.udid = udid
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: String.self)
        try container.encode(ticketNumber, forKey: "numeroTicket")
        try container.encode(udid, forKey: "udid")
        try container.encodeIfPresent(parkingLotCode, forKey: "idGaragem")
        try container.encodeIfPresent(promotionId, forKey: "idPromocao")
        try container.encodeIfPresent(invoices, forKey: "notas")
        try container.encodeIfPresent(promotionsKind, forKey: "tiposPromocao")
    }

}

