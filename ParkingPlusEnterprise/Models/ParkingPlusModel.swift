//
//  ParkingPlusModel.swift
//  ParkingPlusEnterprise
//
//  Created by Duarte, Anderson on 11/09/2018.
//  Copyright © 2018 Duarte, Anderson. All rights reserved.
//

import Foundation

public protocol ParkingPlusModel : Encodable {
    
}

extension String: CodingKey {
    
    public var stringValue: String {
        return self
    }
    
    public init?(stringValue: String) {
        self.init(stringLiteral: stringValue)
    }
    
    public var intValue: Int? {
        return nil
    }
    
    public init?(intValue: Int) {
        return nil
    }
    
}
