import Foundation



open class Promotion: Decodable {

    public enum DiscountType: String, Decodable {
        case percentage = "PORCENTAGEM"
        case value = "VALOR"
        case permanence = "PERMANENCIA"
    }
    public enum Kind: String, Decodable {
        case issuer = "BANDEIRA"
        case coupon = "CUPOM"
    }
    public let systemId: Int64
    /** Titulo da promoção */
    public var title: String
    /** Url para uma imagem para a promoção */
    public var image: String?
    /** A data de validade da promoção */
    public var validity: Date
    /** A descrição da promoção */
    public var description: String
    /** A regulamentação da promoção */
    public var regulation: String?
    /** O tipo da promoção */
    public var kind: Kind
    /** O tipo do desconto */
    public var discountType: DiscountType
    /** Valor necessário para atingir a promoção */
    public var targetValue: Int
    /** Horário em que a promoção pode ser utilizada (inicio) */
    public var timeBegin: Date
    /** Horário em que a promoção pode ser utilizada (fim) */
    public var timeEnd: Date
    /** Caso tipoDesconto; VALOR: valor do desconto; PORCENTAGEM: porcentagem de desconto; PERMANENCIA: tempo em minutos */
    public var discountValue: Int
    /** Bandeira caso o tipoPromoção seja BANDEIRA */
    public var issuer: CreditCard.Issuer?
    /** O nome da promoção (até 15 caracteres) */
    public var name: String
    public let requiresAuth: Bool
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)

        systemId = try container.decode(Int64.self, forKey: "systemId")
        title = try container.decode(String.self, forKey: "titulo")
        image = try container.decodeIfPresent(String.self, forKey: "imagem")
        validity = try container.decode(Date.self, forKey: "validade")
        description = try container.decode(String.self, forKey: "descricao")
        regulation = try container.decodeIfPresent(String.self, forKey: "regulamento")
        kind = try container.decode(Kind.self, forKey: "tipoPromocao")
        discountType = try container.decode(DiscountType.self, forKey: "tipoDesconto")
        targetValue = try container.decode(Int.self, forKey: "valorAlvo")
        timeBegin = try container.decode(Date.self, forKey: "horarioInicio")
        timeEnd = try container.decode(Date.self, forKey: "horarioFim")
        discountValue = try container.decode(Int.self, forKey: "valorDesconto")
        issuer = try container.decodeIfPresent(CreditCard.Issuer.self, forKey: "bandeira")
        name = try container.decode(String.self, forKey: "nome")
        requiresAuth = try container.decode(Bool.self, forKey: "exigeAutenticacao")
    }
}

