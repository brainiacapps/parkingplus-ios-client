//
//  CreditCard.swift
//  ParkingPlusEnterprise
//
//  Created by Duarte, Anderson on 19/09/2018.
//  Copyright © 2018 Duarte, Anderson. All rights reserved.
//

import Foundation

public class CreditCard : Decodable {

    /** Número do cartão de crédito */
    private (set) public var number : String?
    /** Validade do cartão de crédito no formato 'MMYYYY' */
    private (set) public var validity : String?
    /** Código de segurança do cartão de crédito */
    private (set) public var verificationValue : String?
    /** Número do cartão de crédito criptografado */
    public var encryptedValue : String?
    /** Bandeira do cartão de crédito */
    public let issuer : Issuer
    /** Número do documento do cliente para geração do RPS */
    public let holderDocument : String
    /** Obrigatório apenas para Gateway Komerci */
    public let holderName : String
    /** Retornar o cartão criptografado se o pagamento for bem sucedido */
    public var encrypt : Bool?

    public init( issuer: Issuer, number: String, validity: String, verificationValue: String,
          holderDocument: String, holderName: String, encrypt: Bool = false ) {
        self.issuer = issuer
        self.number = number;
        self.validity = validity;
        self.verificationValue = verificationValue;
        self.holderDocument = holderDocument;
        self.holderName = holderName;
        self.encrypt = encrypt
    }
    
    public init( issuer: Issuer, encryptedValue: String, holderDocument: String, holderName: String ) {
        self.issuer = issuer
        self.encryptedValue = encryptedValue;
        self.holderDocument = holderDocument;
        self.holderName = holderName;
    }
    
    public enum Issuer : String, Decodable {
        case VISA = "VISA"
        case MASTERCARD = "MASTERCARD"
        case DINERS_CLUB = "DINERS CLUB"
        case DISCOVER = "DISCOVER"
        case AMERICAN_EXPRESS = "AMERICAN EXPRESS"
        case ELO = "ELO"
        case AURA = "AURA"
        case HIPERCARD = "HIPERCARD"
        case JCB = "JCB"

        public static func from(string: String) -> Issuer? {
            let issuer : Issuer?
            switch string {
            case "VISA":
                issuer = .VISA
            case "MASTERCARD":
                issuer = .MASTERCARD
            case "DINERS_CLUB":
                issuer = .DINERS_CLUB
            case "DISCOVER":
                issuer = .DISCOVER
            case "AMERICAN EXPRESS":
                issuer = .AMERICAN_EXPRESS
            case "ELO":
                issuer = .ELO
            case "AURA":
                issuer = .AURA
            case "HIPERCARD":
                issuer = .HIPERCARD
            case "JCB":
                issuer = .JCB
            default:
                issuer = nil
            }
            return issuer
        }
        /**
         This is a utility however unreliable function to detect the
         credti card issuer. Can be used for suggest to the user the
         issuer but still is recommend that is leave open for the user
         to choose the type of his/her credit card.
       */
        public static func detect( cardNumber : String ) -> Issuer?  {
            var issuer : Issuer?
            let typeAndPatterns : [Issuer : String] = [.VISA: "^4[0-9]{12}(?:[0-9]{3})?$",
                                                       .MASTERCARD: "^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$",
                                                       .DINERS_CLUB: "^3(?:0[0-5]|[68][0-9])[0-9]{11}$",
                                                       .DISCOVER: "^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$",
                                                       .AMERICAN_EXPRESS: "^3[47][0-9]{13}$",
                                                       .ELO: "^(401178|401179|431274|438935|451416|457393|457631|457632|504175|627780|636297|636368|(506699|5067[0-6]\\d|50677[0-8])|(50900\\d|5090[1-9]\\d|509[1-9]\\d{2})|65003[1-3]|(65003[5-9]|65004\\d|65005[0-1])|(65040[5-9]|6504[1-3]\\d)|(65048[5-9]|65049\\d|6505[0-2]\\d|65053[0-8])|(65054[1-9]| 6505[5-8]\\d|65059[0-8])|(65070\\d|65071[0-8])|65072[0-7]|(65090[1-9]|65091\\d|650920)|(65165[2-9]|6516[6-7]\\d)|(65500\\d|65501\\d)|(65502[1-9]|6550[3-4]\\d|65505[0-8]))[0-9]{10,12}",
                                                       .AURA: "^(5078\\d{2})(\\d{2})(\\d{11})$",
                                                       .HIPERCARD: "^(606282\\d{10}(\\d{3})?)|(3841\\d{15})$",
                                                       JCB: "^(?:2131|1800|35\\d{3})\\d{11}$"]
            for (ct, pattern) in typeAndPatterns {
                if (cardNumber.range(of: pattern, options: .regularExpression) != nil) {
                    issuer = ct
                    break
                }
            }
            return issuer;
        }
    }
}
