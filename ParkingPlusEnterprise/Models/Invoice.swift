import Foundation



public class Invoice: Decodable {

    /** CNPJ do emissor */
    public var document: String
    /** Data da nota */
    public var date: Date
    /** Mensagem de erro caso a nota seja inválida */
    public var errorMessage: String
    /** Código QR da nota */
    public var qrCode: String
    /** Se a nota é válida */
    public var isValid: Bool
    /** Valor da nota */
    public var amount: Int64

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)
        document = try container.decode(String.self, forKey: "cnpj")
        date = try container.decode(Date.self, forKey: "data")
        errorMessage = try container.decode(String.self, forKey: "erro")
        qrCode = try container.decode(String.self, forKey: "qrCode")
        isValid = try container.decode(Bool.self, forKey: "valida")
        amount = try container.decode(Int64.self, forKey: "valor")
    }
}

