import Foundation



open class RetornoCreditarCartao: Codable {

    public var comprovante: String?
    /** Código de erro */
    public var errorCode: Int
    /** Mensagem de erro */
    public var mensagem: String
    public var numeroCartao: String?
    public var pagamentoAceito: Bool?
    public var rps: String?
    public var serieRps: String?


    
    public init(comprovante: String?, errorCode: Int, mensagem: String, numeroCartao: String?, pagamentoAceito: Bool?, rps: String?, serieRps: String?) {
        self.comprovante = comprovante
        self.errorCode = errorCode
        self.mensagem = mensagem
        self.numeroCartao = numeroCartao
        self.pagamentoAceito = pagamentoAceito
        self.rps = rps
        self.serieRps = serieRps
    }
    

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {

        var container = encoder.container(keyedBy: String.self)

        try container.encodeIfPresent(comprovante, forKey: "comprovante")
        try container.encode(errorCode, forKey: "errorCode")
        try container.encode(mensagem, forKey: "mensagem")
        try container.encodeIfPresent(numeroCartao, forKey: "numeroCartao")
        try container.encodeIfPresent(pagamentoAceito, forKey: "pagamentoAceito")
        try container.encodeIfPresent(rps, forKey: "rps")
        try container.encodeIfPresent(serieRps, forKey: "serieRps")
    }

    // Decodable protocol methods

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)

        comprovante = try container.decodeIfPresent(String.self, forKey: "comprovante")
        errorCode = try container.decode(Int.self, forKey: "errorCode")
        mensagem = try container.decode(String.self, forKey: "mensagem")
        numeroCartao = try container.decodeIfPresent(String.self, forKey: "numeroCartao")
        pagamentoAceito = try container.decodeIfPresent(Bool.self, forKey: "pagamentoAceito")
        rps = try container.decodeIfPresent(String.self, forKey: "rps")
        serieRps = try container.decodeIfPresent(String.self, forKey: "serieRps")
    }
}

