import Foundation



open class PagamentoCredenciadoRequest: Codable {

    /** Chave calculada para acesso ao serviço. */
    public var apiKey: String
    /** Bandeira do cartão de crédito utilizado no pagamento do ticket. */
    public var bandeira: String
    /** O Cartão criptografado. */
    public var cartaoCriptografado: String?
    /** Número do cartão de crédito a ser utilizado para pagamento. */
    public var cartaoDeCredito: Int64
    /** A chave da garagem. Pode ser obtida pelo método consultarGaragens. */
    public var chaveGaragem: String
    /** Código de segurança do carão de crédito. */
    public var codigoDeSeguranca: String
    /** Número do documento do cliente para geração do RPS. */
    public var cpfCnpj: Int64
    /** Endereço IP do dispositivo conectado ao serviço. */
    public var enderecoIp: String
    /** Senha ou hash da senha do usuário */
    public var hashSenha: String
    /** Um id único para a transação. */
    public var idTransacao: String
    /** O número do cartão de crédito WPS ou matricula. */
    public var matricula: Int64
    /** Nome do portador do cartão de crédito. */
    public var portador: String
    /** Identificador único do dispositivo que acessa o serviço. */
    public var udid: String
    /** Validade do cartão de crédito no formato MMYYYY. */
    public var validade: String
    /** Número inteiro representando o valor a pagar multiplicado por 100. */
    public var valor: Int


    
    public init(apiKey: String, bandeira: String, cartaoCriptografado: String?, cartaoDeCredito: Int64, chaveGaragem: String, codigoDeSeguranca: String, cpfCnpj: Int64, enderecoIp: String, hashSenha: String, idTransacao: String, matricula: Int64, portador: String, udid: String, validade: String, valor: Int) {
        self.apiKey = apiKey
        self.bandeira = bandeira
        self.cartaoCriptografado = cartaoCriptografado
        self.cartaoDeCredito = cartaoDeCredito
        self.chaveGaragem = chaveGaragem
        self.codigoDeSeguranca = codigoDeSeguranca
        self.cpfCnpj = cpfCnpj
        self.enderecoIp = enderecoIp
        self.hashSenha = hashSenha
        self.idTransacao = idTransacao
        self.matricula = matricula
        self.portador = portador
        self.udid = udid
        self.validade = validade
        self.valor = valor
    }
    

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {

        var container = encoder.container(keyedBy: String.self)

        try container.encode(apiKey, forKey: "apiKey")
        try container.encode(bandeira, forKey: "bandeira")
        try container.encodeIfPresent(cartaoCriptografado, forKey: "cartaoCriptografado")
        try container.encode(cartaoDeCredito, forKey: "cartaoDeCredito")
        try container.encode(chaveGaragem, forKey: "chaveGaragem")
        try container.encode(codigoDeSeguranca, forKey: "codigoDeSeguranca")
        try container.encode(cpfCnpj, forKey: "cpfCnpj")
        try container.encode(enderecoIp, forKey: "enderecoIp")
        try container.encode(hashSenha, forKey: "hashSenha")
        try container.encode(idTransacao, forKey: "idTransacao")
        try container.encode(matricula, forKey: "matricula")
        try container.encode(portador, forKey: "portador")
        try container.encode(udid, forKey: "udid")
        try container.encode(validade, forKey: "validade")
        try container.encode(valor, forKey: "valor")
    }

    // Decodable protocol methods

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)

        apiKey = try container.decode(String.self, forKey: "apiKey")
        bandeira = try container.decode(String.self, forKey: "bandeira")
        cartaoCriptografado = try container.decodeIfPresent(String.self, forKey: "cartaoCriptografado")
        cartaoDeCredito = try container.decode(Int64.self, forKey: "cartaoDeCredito")
        chaveGaragem = try container.decode(String.self, forKey: "chaveGaragem")
        codigoDeSeguranca = try container.decode(String.self, forKey: "codigoDeSeguranca")
        cpfCnpj = try container.decode(Int64.self, forKey: "cpfCnpj")
        enderecoIp = try container.decode(String.self, forKey: "enderecoIp")
        hashSenha = try container.decode(String.self, forKey: "hashSenha")
        idTransacao = try container.decode(String.self, forKey: "idTransacao")
        matricula = try container.decode(Int64.self, forKey: "matricula")
        portador = try container.decode(String.self, forKey: "portador")
        udid = try container.decode(String.self, forKey: "udid")
        validade = try container.decode(String.self, forKey: "validade")
        valor = try container.decode(Int.self, forKey: "valor")
    }
}

