import Foundation



open class CriptografarCartaoRequest: Codable {

    public var numeroDoCartao: String?
    public var portador: String?
    public var udid: String?
    public var validade: String?


    
    public init(numeroDoCartao: String?, portador: String?, udid: String?, validade: String?) {
        self.numeroDoCartao = numeroDoCartao
        self.portador = portador
        self.udid = udid
        self.validade = validade
    }
    

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {

        var container = encoder.container(keyedBy: String.self)

        try container.encodeIfPresent(numeroDoCartao, forKey: "numeroDoCartao")
        try container.encodeIfPresent(portador, forKey: "portador")
        try container.encodeIfPresent(udid, forKey: "udid")
        try container.encodeIfPresent(validade, forKey: "validade")
    }

    // Decodable protocol methods

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)

        numeroDoCartao = try container.decodeIfPresent(String.self, forKey: "numeroDoCartao")
        portador = try container.decodeIfPresent(String.self, forKey: "portador")
        udid = try container.decodeIfPresent(String.self, forKey: "udid")
        validade = try container.decodeIfPresent(String.self, forKey: "validade")
    }
}

