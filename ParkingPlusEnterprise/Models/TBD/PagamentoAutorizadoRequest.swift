import Foundation



open class PagamentoAutorizadoRequest: Codable {

    /** Bandeira do cartão de crédito */
    public var bandeira: String
    /** Número do documento do cliente para geração do RPS */
    public var cpfCnpj: Int64?
    /** ID da Garagem referente ao ticket do estacionamento. Caso não utilizado o sistema tentará buscar o ticket em todas as garagens. Retorna erro se mais de um ticket for encontrado */
    public var idGaragem: Int64?
    /** Identificação única para a transação */
    public var idTransacao: String
    /** Número do código de barras do ticket do estacionamento */
    public var numeroTicket: String
    /** Identificador único para o dispositivo */
    public var udid: String
    /** Valor do pagamento em centavos. Exemplo: para um pagamento de R$1,50 enviar 150 */
    public var valor: Int


    
    public init(bandeira: String, cpfCnpj: Int64?, idGaragem: Int64?, idTransacao: String, numeroTicket: String, udid: String, valor: Int) {
        self.bandeira = bandeira
        self.cpfCnpj = cpfCnpj
        self.idGaragem = idGaragem
        self.idTransacao = idTransacao
        self.numeroTicket = numeroTicket
        self.udid = udid
        self.valor = valor
    }
    

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {

        var container = encoder.container(keyedBy: String.self)

        try container.encode(bandeira, forKey: "bandeira")
        try container.encodeIfPresent(cpfCnpj, forKey: "cpfCnpj")
        try container.encodeIfPresent(idGaragem, forKey: "idGaragem")
        try container.encode(idTransacao, forKey: "idTransacao")
        try container.encode(numeroTicket, forKey: "numeroTicket")
        try container.encode(udid, forKey: "udid")
        try container.encode(valor, forKey: "valor")
    }

    // Decodable protocol methods

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)

        bandeira = try container.decode(String.self, forKey: "bandeira")
        cpfCnpj = try container.decodeIfPresent(Int64.self, forKey: "cpfCnpj")
        idGaragem = try container.decodeIfPresent(Int64.self, forKey: "idGaragem")
        idTransacao = try container.decode(String.self, forKey: "idTransacao")
        numeroTicket = try container.decode(String.self, forKey: "numeroTicket")
        udid = try container.decode(String.self, forKey: "udid")
        valor = try container.decode(Int.self, forKey: "valor")
    }
}

