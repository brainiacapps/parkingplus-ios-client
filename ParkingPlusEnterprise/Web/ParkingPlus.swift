//
//  ParkingPlusAPI.swift
//  ParkingPlusEnterprise
//
//  Created by Duarte, Anderson on 05/09/2018.
//  Copyright © 2018 Duarte, Anderson. All rights reserved.
//

import Foundation
import CryptoKit

struct EmptyBody : Encodable {}

public class ParkingPlus {

    public typealias Config = (UDID: String, API_KEY: String, API_HOST: String)
    private let webClient : WebClient
    private let config : Config

    public init(config: Config, urlSession: URLSession = .shared) {
        self.config = config
        self.webClient = WebClient(urlSession: urlSession)
    }
    
    public func pay(ticket number: String, with creditCard: CreditCard, amount: Int,
                          promotionCode: Int64? = nil, parkingLotCode : Int64? = nil, invoices: [String]? = nil,
                          completion: @escaping (ParkingPlusResult<PaymentResult>) -> Void) -> Void {
        let url = config.API_HOST
        let paymentRequest = PaymentRequest(creditCard: creditCard, ticketNumber: number,
                                            amount: amount, ipAddress: "0.0.0.0", udid: self.config.UDID)
        if let promotionCode = promotionCode {
            paymentRequest.promotionCode = promotionCode
        }
        if let parkingLotCode = parkingLotCode {
            paymentRequest.parkingLotCode = parkingLotCode
        }
        if let invoices = invoices {
            paymentRequest.invoices = invoices
        }
        let paymentRequestKey = paymentRequest.getKey(config.API_KEY)
        let apiKeyHashed = Insecure.SHA1.hash(data: paymentRequestKey.data(using: .utf8)!).hexStr
        let service = ParkingPlusService(method: .post, url: url, path: "/pagamento",
                                         responseType: PaymentResult.self, body: paymentRequest)
        .addQueryParam(name: "apiKey", value: apiKeyHashed)
        webClient.request(service) { result in
            switch result {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(ParkingPlusError(code: error.code, message: error.message)))
            }
        }

    }

    public func listPayments(from: Int = 0, to: Int = 5, parkingLotCode : Int64? = nil, completion: @escaping (ParkingPlusResult<[Payment]>) -> Void) -> Void {
        let url = config.API_HOST
        let udid = config.UDID
        let apiKeyHashed = Insecure.SHA1.hash(data: (udid + config.API_KEY).data(using: .utf8)!).hexStr
        let service = ParkingPlusService<EmptyBody, [Payment]>(method: .get, url: url,
                                                               path: "/pagamentosEfetuados", responseType: [Payment].self)
            .addQueryParam(name: "apiKey", value: apiKeyHashed)
            .addQueryParam(name: "udid", value: config.UDID)
            .addQueryParam(name: "inicio", value: from.description)
            .addQueryParam(name: "limite", value: to.description)
        if let parkingLotCode = parkingLotCode {
            _ = service.addQueryParam(name: "idGaragem", value: String(parkingLotCode))
        }
        webClient.request(service) { result in
            switch result {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(ParkingPlusError(code: error.code, message: error.message)))
            }
        }
    }

    public func getTicketInfo(number: String, parkingLotCode : Int64? = nil, completion: @escaping (ParkingPlusResult<ParkingTicket>) -> Void) -> Void {
        let url = config.API_HOST
        let udid = config.UDID
        let apiKeyHashed = Insecure.SHA1.hash(data: (udid + config.API_KEY).data(using: .utf8)!).hexStr
        let service = ParkingPlusService<EmptyBody, ParkingTicket>(method: .get, url: url, path: "/ticket/{number}", responseType: ParkingTicket.self)
            .addPathParam(name: "number", value: number)
            .addQueryParam(name: "apiKey", value: apiKeyHashed)
            .addQueryParam(name: "udid", value: config.UDID)
        if let parkingLotCode = parkingLotCode {
            _ = service.addQueryParam(name: "idGaragem", value: String(parkingLotCode))
        }
        webClient.request(service) { result in
            switch result {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(ParkingPlusError(code: error.code, message: error.message)))
            }
        }
    }

    public func refreshTicket(ticketNumber: String, parkingLotCode: Int64? = nil, promotionId: Int64? = nil,
                              invoices: [String]? = nil, promotionsKind: [String]? = nil,
                              completion: @escaping (ParkingPlusResult<ParkingTicket>) -> Void) -> Void {
        let url = config.API_HOST
        let udid = config.UDID
        let apiKeyHashed = Insecure.SHA1.hash(data: (udid + config.API_KEY).data(using: .utf8)!).hexStr
        let ticketRefreshRequest = TicketRefreshRequest(ticketNumber: ticketNumber, udid: udid, parkingLotCode: parkingLotCode, promotionId: promotionId, invoices: invoices, promotionsKind: promotionsKind)
        let service = ParkingPlusService(method: .post, url: url, path: "/ticket",
                                         responseType: ParkingTicket.self, body: ticketRefreshRequest)
            .addQueryParam(name: "apiKey", value: apiKeyHashed )
        webClient.request(service) { result in
            switch result {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(ParkingPlusError(code: error.code, message: error.message)))
            }
        }
    }

    public func listPromotions(ticketNumber: String, parkingLotCode: Int64, completion: @escaping (ParkingPlusResult<[Promotion]>) -> Void) -> Void {
        let url = config.API_HOST
        let apiKeyHashed = Insecure.SHA1.hash(data: config.API_KEY.data(using: .utf8)!).hexStr
        let service = ParkingPlusService<EmptyBody, [Promotion]>(method: .get, url: url,
                                                               path: "/promocoes", responseType: [Promotion].self)
            .addQueryParam(name: "apiKey", value: apiKeyHashed)
            .addQueryParam(name: "idGaragem", value: String(parkingLotCode))
            .addQueryParam(name: "numeroTicket", value: ticketNumber)
        webClient.request(service) { result in
            switch result {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(ParkingPlusError(code: error.code, message: error.message)))
            }
        }
    }
    
    //    public static func rechargeCreditCard() -> ParkingPlusService {
    //        return ParkingPlusService(method: .post, path: "/carregarCredenciado")
    //    }
    //
    //    public static func rechargeDebitCard() -> ParkingPlusService {
    //        return ParkingPlusService(method: .post, path: "/creditarCartao")
    //    }
    //
    //    public static func payTicketWithoutAuthorization() -> ParkingPlusService {
    //        return ParkingPlusService(method: .post, path: "/pagamentoAutorizado")
    //    }
    //
}

public enum ParkingPlusResult<T> {
    case success(T)
    case failure(ParkingPlusError)
}

class ParkingPlusService<Body: Encodable, Response : Decodable> : WebRequest {
    private let method : Method
    private let url : String
    private let path : String
    private let responseType : Response.Type
    private let requestBody : Body?
    private var pathParams = [String:String]()
    private var queryParams = [String:String]()
    private var headers = [String:String]()
    
    public init(method: Method, url: String, path: String, responseType: Response.Type, body: Body? = nil) {
        self.method = method
        self.url = url
        self.path = path
        self.responseType = responseType
        self.requestBody = body
        self.headers["Content-Type"] = "application/json"
    }
    
    enum Method: String {
        case post
        case get
        case put
        case delete
        case patch
    }
    
    func createRequest() throws -> URLRequest {
        var urlComponents = URLComponents(string: url)!
        urlComponents.queryItems = queryParams.map({URLQueryItem(name: $0, value: $1)})
        urlComponents.path = urlComponents.path + getPath()
        var request = URLRequest(url: urlComponents.url!)
        if let body = requestBody {
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .millisecondsSince1970
            request.httpBody = try encoder.encode(body)
        }
        request.httpMethod = method.rawValue.uppercased()
        request.allHTTPHeaderFields = headers
        return request
    }
    
    func parseResponse(data: Data) throws -> Response {
        let coder = JSONDecoder()
        coder.dateDecodingStrategy = .millisecondsSince1970
        return try coder.decode(responseType, from: data)
    }

    fileprivate func getPath() -> String {
        var parsedPath = path
        for (key, value) in pathParams {
            parsedPath = parsedPath.replacingOccurrences(of: key, with: value)
        }
        return parsedPath
    }
    
    fileprivate func addPathParam(name: String, value: String) -> ParkingPlusService {
        pathParams["{" + name + "}"] = value
        return self
    }

    fileprivate func addQueryParam(name: String, value: String) -> ParkingPlusService {
        queryParams[name] = value
        return self
    }
    
}

extension Digest {
    var bytes: [UInt8] { Array(makeIterator()) }
    var data: Data { Data(bytes) }

    var hexStr: String {
        bytes.map { String(format: "%02X", $0) }.joined()
    }
}

