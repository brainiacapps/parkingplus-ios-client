//
//  APIRequest.swift
//  ParkingPlusEnterprise
//
//  Created by Duarte, Anderson on 16/09/2018.
//  Copyright © 2018 Duarte, Anderson. All rights reserved.
//

import Foundation

protocol WebRequest {
    associatedtype ResponseDataType : Decodable
    func createRequest() throws -> URLRequest
    func parseResponse(data: Data) throws -> ResponseDataType
}

enum WebRequestResult<T> {
    case success(T)
    case failure(WebRequestError)
}

let INVALID_RESPONSE = WebRequestError(code: 500, message: "Invalid response from server.")

class WebRequestError: Error, Decodable {

    public let code: Int
    public let message: String
    
    public init(code: Int, message: String) {
        self.code = code
        self.message = message
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: String.self)
        code = try container.decode(Int.self, forKey: "errorCode")
        message = try container.decode(String.self, forKey: "mensagem")
    }

}

class WebClient {

    let urlSession: URLSession
    init(urlSession: URLSession) {
        self.urlSession = urlSession
    }
    
    func request<T : WebRequest>(_ service : T, completionHandler: @escaping (WebRequestResult<T.ResponseDataType>) -> Void) -> Void {
        do {
            let url = try service.createRequest()
            let task = urlSession.dataTask(with: url) { data, response, error in
                do {
                    if let error = error {
                        return completionHandler(.failure(WebRequestError(code: 500, message: error.localizedDescription)))
                    }
                    guard let httpResponse = response as? HTTPURLResponse, let data = data else {
                        return completionHandler(.failure(INVALID_RESPONSE))
                    }
                    switch httpResponse.statusCode {
                        case 200:
                            completionHandler(.success(try service.parseResponse(data: data)))
                        case 400...499:
                            let error = try JSONDecoder().decode(WebRequestError.self, from: data)
                            completionHandler(.failure(error))
                        default:
                            completionHandler(.failure(INVALID_RESPONSE))
                    }
                }
                catch let error {
                    completionHandler(.failure(WebRequestError(code: 500, message: error.localizedDescription)))
                }
            }
            task.resume()
        }
        catch let error {
            completionHandler(.failure(WebRequestError(code: 500, message: error.localizedDescription)))
        }
    }
}
